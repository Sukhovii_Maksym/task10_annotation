package model;

import java.lang.reflect.InvocationTargetException;

public class MyClass<T> {
    private Object object;
    private String smth;
    private T data;
    private final Class<T> clazz;


    public MyClass(Class<T> clazz) {
        this.clazz = clazz;
        try {
            data = clazz.getConstructor().newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public MyClass(Object object, String smth, T data, Class<T> clazz) {
        this.object = object;
        this.smth = smth;
        this.data = data;
        this.clazz = clazz;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getSmth() {
        return smth;
    }

    public void setSmth(String smth) {
        this.smth = smth;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
