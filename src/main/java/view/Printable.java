package view;

import java.lang.reflect.InvocationTargetException;

@FunctionalInterface
public interface Printable {

    void print() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException;
}
