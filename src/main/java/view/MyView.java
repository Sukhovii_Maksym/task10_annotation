package view;

import annotation.PersonAnnotation;
import annotation.StringAnnotation;
import model.Gender;
import model.MyClass;
import model.Person;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Class cls = Person.class;
    Person person = new Person();


    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Show class annotation with values");
        menu.put("2", "  2 - Show annotated fields");
        menu.put("3", "  3 - Show methods");
        menu.put("4", "  4 - Invoke method with one argument");
        menu.put("5", "  5 - Invoke method with undefined count of parameters");
        menu.put("6", "  6 - Show all info about MyClass<T>");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        System.out.println(cls.getAnnotation(PersonAnnotation.class));
    }

    private void pressButton2() {
        System.out.println("Annotated fields:");
        for (Field clsField : cls.getDeclaredFields()) {
            if (clsField.isAnnotationPresent(StringAnnotation.class)) {
                System.out.println(clsField.getName());
            }
        }
    }

    private void pressButton3() {
        Method[] methods = cls.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("(" + method.getReturnType() + ")" + method.getName() +
                    Arrays.toString(method.getParameterTypes()));
        }
    }

    private void pressButton4() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        person.setName("John");
        person.setSurname("Smith");
        Method method = cls.getDeclaredMethod("says", String.class);
        String hello = "Hello!!!";
        method.invoke(person, hello);

        cls.getDeclaredMethod("setAge", int.class)
                .invoke(person, 41);
        cls.getDeclaredMethod("setGender", Gender.class)
                .invoke(person, Gender.MALE);
        cls.getDeclaredMethod("setPassport", String.class)
                .invoke(person, "SP34598");
        System.out.println(person);

    }


    private void pressButton5() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (person.getName() == null) person.setName("John");
        if (person.getSurname() == null) person.setSurname("Smith");
        String[] strings = {"Java ", "are more ", "flexible and powerful ", "with Reflection!!!"};
        Method methodArgs = cls.getDeclaredMethod("talks", String[].class);
        methodArgs.setAccessible(true);
        methodArgs.invoke(person, (Object) strings);

    }

    private void pressButton6() {
        Class genericClass = MyClass.class;

        Field[] fields = genericClass.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }
        Method[] methods = genericClass.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);

        }

    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
